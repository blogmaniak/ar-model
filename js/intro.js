$(document).ready(function(){
	$.get('/php/data.php',function(data){
		
	});

	var leftSliderDragging = false;
	var rightSliderDragging = false;

	$('#left_slider').mousedown(function() {
		leftSliderDragging = true;
	});

	$('#right_slider').mousedown(function() {
		rightSliderDragging = true;
	});

	$(document).mousemove(function(e) {
		if (leftSliderDragging) {
			moveLeftSlider(e);
		}
		if (rightSliderDragging) {
			moveRightSlider(e);
		}
	});

	$(document).mouseup(function() {
		leftSliderDragging = false;
		rightSliderDragging = false;
	});

	function moveLeftSlider(e) {
		this.svgRatio = window.innerWidth*0.4/500;
		this.mouseLocation = e.pageX/this.svgRatio-75.8285;
		this.attrX = 72.4195;
		this.attrRx = 7.48;
		this.attrWidth = 452.53;
		var rightSliderTransform = parseInt($('#right_slider .cls-12').attr('cx'));
		if ($('#right_slider').attr('transform')) {
			rightSliderTransform += parseFloat($('#right_slider').attr('transform').slice(10, -3));
		}
		if (this.mouseLocation < this.attrX + this.attrRx) {
			this.mouseLocation = this.attrX + this.attrRx;
		} else if (this.mouseLocation > rightSliderTransform) {
			this.mouseLocation = rightSliderTransform;
		}
		this.xPos = this.mouseLocation - parseInt($('#left_slider .cls-12').attr('cx'));
		$('#left_slider').attr('transform', 'translate('+this.xPos+',0)');
		$('#idealBar').attr('d', 'M'+(this.mouseLocation)+',149.28H'+(rightSliderTransform+1)+'a7,7,0,0,1,7,7v1a7,7,0,0,1-7,7H'+(this.mouseLocation)+'a7.48,7.48,0,0,1-7.48-7.48v0a7.48,7.48,0,0,1,7.48-7.48Z');
		$('#earlyBar').attr('d', 'M81.94,149.28H'+(this.mouseLocation-1)+'a7,7,0,0,1,7,7v1a7,7,0,0,1-7,7H81.94a7.48,7.48,0,0,1-7.48-7.48v0A7.48,7.48,0,0,1,81.94,149.28Z');
		earlyIdealLimit = parseInt(Math.ceil(maxDaysToPay/20)*20*(this.mouseLocation - this.attrX)/(this.attrWidth-this.attrX));
		d3.select('#graphSvg').selectAll('circle')
			.style('fill', function(d) {
				return colorScale(d.daysToPay)[0];
			});
		d3.select('#secondaryGraph').selectAll('circle')
			.style('fill', function(d) {
				this.color = colorScale(d.daysToPay)[1];
				if ((this.color == 'red' && latestPayingClients) ||
					(this.color == 'green' && earliestPayingClients)) {
					return colorScale(d.daysToPay)[0];
				} else {
					return 'rgba(255, 255, 255, 0)';
				}
			});
	};

	function moveRightSlider(e) {
		this.svgRatio = window.innerWidth*0.4/500;
		this.mouseLocation = e.pageX/this.svgRatio-75.8285;
		this.attrX = 72.4195;
		this.attrRx = 7.48;
		this.attrWidth = 452.53;
		var leftSliderTransform = parseInt(($('#left_slider .cls-12').attr('cx')));
		if ($('#left_slider').attr('transform')) {
			leftSliderTransform += parseFloat($('#left_slider').attr('transform').slice(10, -3));
		}
		if (this.mouseLocation < leftSliderTransform) {
			this.mouseLocation = leftSliderTransform;
		} else if (this.mouseLocation > this.attrX + this.attrWidth - this.attrRx) {
			this.mouseLocation = this.attrX + this.attrWidth - this.attrRx;
		}
		this.xPos = this.mouseLocation - parseInt($('#right_slider .cls-12').attr('cx'));
		$('#right_slider').attr('transform', 'translate('+this.xPos+',0)');
		$('#idealBar').attr('d', 'M'+(leftSliderTransform)+',149.28H'+(this.mouseLocation+1)+'a7,7,0,0,1,7,7v1a7,7,0,0,1-7,7H'+parseInt(leftSliderTransform)+'a7.48,7.48,0,0,1-7.48-7.48v0a7.48,7.48,0,0,1,7.48-7.48Z');
		$('#lateBar').attr('d', 'M'+(this.mouseLocation+1)+',149.28H519.51a7,7,0,0,1,7,7v1a7,7,0,0,1-7,7H'+(this.mouseLocation+1)+'a7.48,7.48,0,0,1-7.48-7.48v0a7.48,7.48,0,0,1,7.48-7.48Z');
		lateIdealLimit = parseInt(Math.ceil(maxDaysToPay/20)*20*(this.mouseLocation - this.attrX)/(this.attrWidth-this.attrX));
		d3.select('#graphSvg').selectAll('circle')
			.style('fill', function(d) {
				return colorScale(d.daysToPay)[0];
			});
		d3.select('#secondaryGraph').selectAll('circle')
			.style('fill', function(d) {
				this.color = colorScale(d.daysToPay)[1];
				if ((this.color == 'red' && latestPayingClients) ||
					(this.color == 'green' && earliestPayingClients)) {
					return colorScale(d.daysToPay)[0];
				} else {
					return 'rgba(255, 255, 255, 0)';
				}
			});
	};


	$('#clientsButton').click(function(){
		state = 0;
		clients = true;
		updateClientsData();
		$('#graphSvg').remove();
		showGraph('#graphSvgDiv', clientsData);
		$('#graphSvg').show();
		$('#graphImg').hide();
		$('#graphImg').attr("src","");

		$('#clients').show();
		$('#invoices').hide();
		// $('.invoices_dash').hide();
		// $('.clients_dash').show();
	});
	$('#invoicesButton').click(function(){
		state = 0;
		clients = false;
		updateInvoicesData();
		$('#graphSvg').remove();
		showGraph('#graphSvgDiv', invoicesData);
		$('#graphSvg').show();
		$('#graphImg').hide();
		$('#graphImg').attr("src","");

		$('#clients').hide();
		$('#invoices').show();
	// 	$('.invoices_dash').show();
	// 	$('.clients_dash').hide();
	});

	$('#latestButton').click(function() {
		latestPayingClients = true;
		earliestPayingClients = false;
		d3.select('#secondaryGraph').selectAll('circle')
			.style('fill', function(d) {
				this.color = colorScale(d.daysToPay)[1];
				if (this.color == 'red') {
					return colorScale(d.daysToPay)[0];
				} else {
					return 'rgba(255, 255, 255, 0)';
				}
			});
	});

	$('#earliestButton').click(function() {
		earliestPayingClients = true;
		latestPayingClients = false;
		d3.select('#secondaryGraph').selectAll('circle')
			.style('fill', function(d) {
				this.color = colorScale(d.daysToPay)[1];
				if (this.color == 'green') {
					return colorScale(d.daysToPay)[0];
				} else {
					return 'rgba(255, 255, 255, 0)';
				}
			});
	});


	// var jsonData = 
	// {
	//     "feb": [
	//         {
 //  	            "client": "CIBC", 
	//             "amount": "100000",
	//             "daysToPay": "6",
	//             "month": "feb"
	//         },
	//         {
 //  	            "client": "CIBC", 
	//             "amount": "100000",
	//             "daysToPay": "4",
	//             "month": "feb"
	//         },
	//         {
 //  	            "client": "BMO", 
	//             "amount": "100000",
	//             "daysToPay": "7",
	//             "month": "feb"
	//         },
	//         {
 //  	            "client": "RBC", 
	//             "amount": "100000",
	//             "daysToPay": "5",
	//             "month": "feb"
	//         },
	//         {
 //  	            "client": "RBC", 
	//             "amount": "500000",
	//             "daysToPay": "60",
	//             "month": "feb"
	//         }
	//     ],
	//     "dec": [
	//         {
 //  	            "client": "RBC", 
	//             "amount": "700000",
	//             "daysToPay": "32",
	//             "month": "dec"
	// 		},
	//         {
	//         	"client": "BMO",
	//             "amount": "700000",
	//             "daysToPay": "66",
	//             "month": "dec"
	//         }
	//     ],
	//     "jul": [
	// 		{
 //  	            "client": "RBC", 
	//             "amount": "3.46",
	//             "daysToPay": "86",
	//             "month": "jul"
	//         }
	//     ],
	//     "sep": [
	//         {
	//         	"client": "BMO",
	//             "amount": "3.43",
	//             "daysToPay": "16",
	//             "month": "sep"
	//         },
	//         {
 //  	            "client": "HSBC", 
	//             "amount": "900000",
	//             "daysToPay": "46",
	//             "month": "sep"
	//         },
	//         {
 //  	            "client": "HSBC", 
	//             "amount": "900000",
	//             "daysToPay": "11",
	//             "month": "sep"
	//         }
	//     ],
	//     "may": [
	//         {
 //  	            "client": "BMO", 
	//             "amount": "300000",
	//             "daysToPay": "7",
	//             "month": "may"
	//         },
	//         {
 //  	            "client": "BMO", 
	//             "amount": "300000",
	//             "daysToPay": "5",
	//             "month": "may"
	//         },
	//     ],
	//     "maxAmount": "900000",
	//     "minDaysToPay": "4",
	//     "maxDaysToPay": "86"
	// };

	var jsonData = {};
	var clientsData = [];
	var invoicesData = [];

	var latestPayingClients = true;
	var earliestPayingClients = false;
	var categorizedClientsData = {};
	var categorizedInvoicesData = {};
	var maxAmount = 0;
	var maxDaysToPay = 0;
	var earlyIdealLimit = 0;
	var lateIdealLimit = 0;

	$.get( "http://104.196.24.187:9030/clients", function( data ) {
		jsonData = JSON.parse(data);
		maxAmount = parseFloat(jsonData.maxAmount);
		maxDaysToPay = parseFloat(jsonData.maxDaysToPay);
		earlyIdealLimit = Math.ceil(maxDaysToPay/20)*8;
		lateIdealLimit = Math.ceil(maxDaysToPay/20)*12;
		initializeData();
	});

	function makeClientsData() {
		Object.keys(jsonData).forEach(function(i) {
			if (i != "maxAmount" && i != "minDaysToPay" && i != "maxDaysToPay") {
				jsonData[i].forEach(function(j) {
					var amount = maxAmount/10*Math.floor(j.amount/(maxAmount/10)) + maxAmount/20;
					if (amount == maxAmount + maxAmount/20) {
						amount -= maxAmount/10;
					}
					if (categorizedClientsData[i]) {
						if (categorizedClientsData[i][amount]) {
							if (categorizedClientsData[i][amount][j.client]) {
								categorizedClientsData[i][amount][j.client].push(j.daysToPay);
							} else {
								categorizedClientsData[i][amount][j.client] = [j.daysToPay];
							}
						} else {
							categorizedClientsData[i][amount] = {};
							categorizedClientsData[i][amount][j.client] = [j.daysToPay];
						}
					} else {
						categorizedClientsData[i] = {};
						categorizedClientsData[i][amount] = {};
						categorizedClientsData[i][amount][j.client] = [j.daysToPay];
					}
				});
			}
		});

		updateClientsData();
	};

	function updateClientsData() {
		Object.keys(categorizedClientsData).forEach(function(i) {
			Object.keys(categorizedClientsData[i]).forEach(function(j) {
				var sumOfAvgDaysToPayForClient = 0;
				var count = 0;
				Object.keys(categorizedClientsData[i][j]).forEach(function(k) {
					var sum = 0;
					categorizedClientsData[i][j][k].forEach(function(l) {
						sum += parseInt(l);
					});
					avgDaysToPayForClient = sum/categorizedClientsData[i][j][k].length
					sumOfAvgDaysToPayForClient += avgDaysToPayForClient;
					count += 1;
				});
				avgDaysToPay = sumOfAvgDaysToPayForClient/count;
				clientsData.push({
					"amount": j,
					"daysToPay": avgDaysToPay,
					"numOfClientsOrInvoices": count,
					"month": i
				});
			});
		});
	}

	function makeInvoicesData() {
		Object.keys(jsonData).forEach(function(i) {
			if (i != "maxAmount" && i != "minDaysToPay" && i != "maxDaysToPay") {
				jsonData[i].forEach(function(j) {
					var amount = maxAmount/10*Math.floor(j.amount/(maxAmount/10)) + maxAmount/20;
					if (amount == maxAmount + maxAmount/20) {
						amount -= maxAmount/10;
					}
					if (categorizedInvoicesData[i]) {
						if (categorizedInvoicesData[i][amount]) {
							categorizedInvoicesData[i][amount].push(j.daysToPay);
						} else {
							categorizedInvoicesData[i][amount] = [j.daysToPay];
						}
					} else {
						categorizedInvoicesData[i] = {};
						categorizedInvoicesData[i][amount] = [j.daysToPay];
					}
				});
			}
		});

		updateInvoicesData();
	};

	function updateInvoicesData() {
		Object.keys(categorizedInvoicesData).forEach(function(i) {
			Object.keys(categorizedInvoicesData[i]).forEach(function(j) {
				var sum = 0;
				var count = 0;
				categorizedInvoicesData[i][j].forEach(function(k) {
					sum += parseInt(k);
					count += 1;
				});
				var avgDaysToPay = sum/count;
				invoicesData.push({
					"amount": j,
					"daysToPay": avgDaysToPay,
					"numOfClientsOrInvoices": count,
					"month": i
				});
			});
		});
	};

	function colorScale(daysToPay) {
		this.idealLimitRange = lateIdealLimit - earlyIdealLimit;
		this.idealRangeAvg = (earlyIdealLimit + lateIdealLimit)/2;
		this.minDaysToPay = jsonData.minDaysToPay;
		this.maxDaysToPay = jsonData.maxDaysToPay;
		this.rgba = '';
		this.color = '';
		earlyColorScale = d3.scale.linear()
			.domain([this.minDaysToPay, earlyIdealLimit])
			.range([1, 0.5]);
		lateColorScale = d3.scale.linear()
			.domain([this.maxDaysToPay, lateIdealLimit])
			.range([1, 0.5]);
		if (daysToPay < earlyIdealLimit) {
			this.rgba = 'rgba(0, 255, 0,' + earlyColorScale(daysToPay) + ')';
			this.color = 'green';
		} else if (daysToPay > lateIdealLimit) {
			this.rgba = 'rgba(255, 0, 0,' + lateColorScale(daysToPay) + ')';
			this.color = 'red';
		} else {
			this.rgba = 'rgba(0, 0, 255, 0.5)';
			this.color = 'blue';
		}
		return [this.rgba, this.color];
	}

	function initializeData() {
		makeClientsData();
		makeInvoicesData();
		
		showGraph('#graphSvgDiv', clientsData);
		showGraph('#secondaryGraphSvgDiv', clientsData);
		d3.select('#secondaryGraph').selectAll('circle')
			.style('fill', function(d) {
				this.color = colorScale(d.daysToPay)[1];
				if (this.color == 'red') {
					return colorScale(d.daysToPay)[0];
				} else {
					return 'rgba(255, 255, 255, 0)';
				}
			});
		makeDueDateAxis();
	};

	function showGraph(id, data) {
		var svgId = '';
		if(id == '#graphSvgDiv'){
			svgId = 'graphSvg';
		}
		else if(id == '#secondaryGraphSvgDiv'){
			svgId = 'secondaryGraphSvg';
		}
		var width = parseInt($(id).css('width'));
		var height = parseInt($(id).css('height'));
		var padding = 60;

		var svgContainer = d3.select(id).append('svg')
			.attr('id', svgId)
			.attr('width', '100%')
			.attr('height', '100%');

		var monthsScale = d3.scale.ordinal()
			.domain(['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'])
			.range(['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);


		var xAxisScale = d3.scale.ordinal()
			.domain(['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'])
			.rangeBands([padding, width - padding]);

		var yAxisScale = d3.scale.linear()
			.domain([0, maxAmount])
			.range([height - padding, padding]);

		var xAxis = d3.svg.axis()
			.scale(xAxisScale);

		var yAxis = d3.svg.axis()
			.scale(yAxisScale)
			.orient('left')
			.ticks(10);

		var xAxisGroup = svgContainer.append('g')
			.attr('class', 'axis')
			.attr('transform', 'translate(0,' + (height - padding) + ')')
			.call(xAxis);

		var yAxisGroup = svgContainer.append('g')
			.attr('class', 'axis')
			.attr('transform', 'translate(' + padding + ',0)')
			.call(yAxis);
		maxNumOfClientsOrInvoices = d3.max(data, function(d) { return d.numOfClientsOrInvoices; })
		svgContainer.selectAll('circle')
			.data(data)
			.enter()
			.append('circle')
			.attr('cx', function(d) {
				return xAxisScale(monthsScale(d.month)) + width/26;
			})
			.attr('cy', function(d) {
				return yAxisScale(d.amount);
			})
			.attr('r', function(d) {
				return parseFloat($('body').width())/36.57*Math.sqrt(d.numOfClientsOrInvoices/maxNumOfClientsOrInvoices);
			})
			.attr('fill', function(d) {
				return colorScale(d.daysToPay)[0];
			})
			.attr('class', 'graphDataPoints');
	}

	window.onresize = function() {
		d3.select('#graphSvg').selectAll('circle')
			.attr('r', function(d) {
				return parseFloat($('body').width())/36.57*Math.sqrt(d.numOfClientsOrInvoices/maxNumOfClientsOrInvoices);
			});
	};

	function makeDueDateAxis() {
		var dueDateScale = d3.scale.linear()
			.domain([0, Math.ceil(maxDaysToPay/20)*20])
			.range([81.94, 519.51]);

		var dueDateAxis = d3.svg.axis()
			.scale(dueDateScale)
			.orient("top")
			.ticks(6)
			.tickFormat(function(d) {
				return d + ' Days';
			});

		var dueDateAxisGroup = d3.select('#region').append('g')
			.attr('id', 'dueDateAxis')
			.attr('transform', 'translate(0,145)')
			.call(dueDateAxis);
	} 

});